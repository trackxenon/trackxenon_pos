{
    'name': 'TrackXenon POS',
    'version': '1.1',
    'website': 'https://www.odoo.com/page/project-management',
    'category': 'Project',
    'sequence': 10,
    'summary': 'Projects, Tasks',
    'depends': ['hiworth_hr_attendance','account_voucher','point_of_sale',
        'hiworth_accounting','account','purchase','stock','account_voucher','mail','hr','point_of_sale','document','hr_holidays'
    ],
    'description': """
..
    """,
    'data': [
     'views/hide_pos.xml',
     'views/menu_pos.xml',
     'views/module_hide.xml',
     'views/stock_report.xml',
     'views/commission_details.xml',
    
    ],
    
    'installable': True,
    'auto_install': False,
    'application': True,
}
