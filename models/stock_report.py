from openerp import fields, models, api
import datetime, calendar
from openerp.osv import osv
from decimal import Decimal


class StockMoveReportWizard(models.TransientModel):
    _name='stock.move.report.wizard'

    from_date=fields.Date(default=lambda self: self.default_time_range('from'))
    to_date=fields.Date(default=lambda self: self.default_time_range('to'))
    source=fields.Many2one('stock.location', 'Location')
    destination=fields.Many2one('stock.location', 'Destination')
    
    

    # Calculate default time ranges
    @api.model
    def default_time_range(self, type):
        year = datetime.date.today().year
        month = datetime.date.today().month
        last_day = calendar.monthrange(datetime.date.today().year,datetime.date.today().month)[1]
        first_day = 1
        if type=='from':
            return datetime.date(year, month, first_day)
        elif type=='to':
            return datetime.date(year, month, last_day)

    @api.multi
    def print_stock_move_report(self):
        self.ensure_one()
        stockmove = self.env['stock.move']
        if self.source.id  :
            stockmoverecs = stockmove.search([('state','=','done'),('location_id','=',self.source.id)])
        # if self.source.id != False and self.destination.id == False:
        #     stockmoverecs = stockmove.search([('date','>=',self.from_date),('date','<=',self.to_date),('state','=','done'),
        #                                           ('location_id','=',self.source.id)])
        # if self.source.id == False and self.destination.id != False:
        #     stockmoverecs = stockmove.search([('date','>=',self.from_date),('date','<=',self.to_date),('state','=','done'),
        #                                           ('location_dest_id','=',self.destination.id)])
        # if self.source.id == False and self.destination.id == False:
        #     stockmoverecs = stockmove.search([('date','>=',self.from_date),('date','<=',self.to_date),('state','=','done')])
        
        if not stockmoverecs:
            raise osv.except_osv(('Error'), ('There are no material requests to display. Please make sure material requests exist.'))
#        
                
        recordset = stockmoverecs.sorted(key=lambda r: r.date)
        datas = {
            'ids': self._ids,
            'model': self._name,
            'form': self.read(),
            'context':self._context,
        }
        return{
            'type' : 'ir.actions.report.xml',
            'report_name' : 'trackxenon_pos.report_stock_move_template_view',
            'datas': datas,
#             'context':{'start_date': self.from_date, 'end_date': self.to_date,'source': self.source.name,'destination': self.destination.name}
            'report_type': 'qweb-pdf',
        }
        
        
        
    @api.multi
    def view_stock_move_report(self):
        self.ensure_one()
        stockmove = self.env['stock.move']
        if self.source.id :
            stockmoverecs = stockmove.search([('state','=','done'),('location_id','=',self.source.id)])
        
        if not stockmoverecs:
            raise osv.except_osv(('Error'), ('There are no material requests to display. Please make sure material requests exist.'))
        
        recordset = stockmoverecs.sorted(key=lambda r: r.date)
        datas = {
            'ids': self._ids,
            'model': self._name,
            'form': self.read(),
            'context':self._context,
        }
        return{
            'type' : 'ir.actions.report.xml',
            'report_name' : 'trackxenon_pos.report_stock_move_template_view',
            'datas': datas,
#             'context':{'start_date': self.from_date, 'end_date': self.to_date,'source': self.source.name,'destination': self.destination.name}
            'report_type': 'qweb-html',
        }


    @api.multi
    def get_producs(self):
        self.ensure_one()
        stockmove = self.env['stock.move']
        
#         
        
        if self.source.id :
            stockmoverecs = stockmove.search([('state','=','done'),('location_id','=',self.source.id)])

        recordset = stockmoverecs.sorted(key=lambda r: r.date)
        
        product_ids = []
        product_ids = [record.product_id.id for record in recordset] 
        product_ids = set(product_ids)
        product_ids = list(product_ids)
        print 'product_ids====================',product_ids

        product_objects = self.env['product.product'].search([('id','in', product_ids)])
        
        for product in product_objects:
            product.tmp_stock_qty = 0.0
            product.temp_stock_value = 0.0
            product.temp_avg_price = 0.0
            if self.source.id:
                recs = stockmove.search([('date','>=',self.from_date),('date','<=',self.to_date),('state','=','done'),
                                                  ('product_id','=',product.id),('location_id','=',self.source.id)])
                print 'rescccccccccccccccccccccccccccccccccccccccccccccccc',recs
                for record in recs :
                    product.tmp_stock_qty += record.product_uom_qty
                    product.temp_stock_value += record.product_uom_qty*record.price_unit
            # if self.source.id != False and self.destination.id == False:
            #     recs = stockmove.search([('date','>=',self.from_date),('date','<=',self.to_date),('state','=','done'),
            #                                        ('product_id','=',product.id),('location_id','=',self.source.id)])
            #     for record in recs :
            #         product.tmp_stock_qty += record.product_uom_qty
            #         product.temp_stock_value += record.product_uom_qty*record.price_unit
            # if self.source.id == False and self.destination.id != False:
            #     recs = stockmove.search([('date','>=',self.from_date),('date','<=',self.to_date),('state','=','done'),
            #                                        ('product_id','=',product.id),('location_dest_id','=',self.destination.id)])
            #     for record in recs :
            #         product.tmp_stock_qty += record.product_uom_qty
            #         product.temp_stock_value += record.product_uom_qty*record.price_unit
            # if self.source.id == False and self.destination.id == False:
            #     recs = stockmove.search([('date','>=',self.from_date),('date','<=',self.to_date),
            #                                 ('product_id','=',product.id),('state','=','done')])
            #     for record in recs :
            #         product.tmp_stock_qty += record.product_uom_qty
            #         product.temp_stock_value += record.product_uom_qty*record.price_unit
            temp_avg_price_decimal = Decimal(product.temp_stock_value/product.tmp_stock_qty)
            product.temp_avg_price = round(temp_avg_price_decimal,2) 
        return product_objects
    