from openerp import fields, models, api
import datetime, calendar
from openerp.osv import osv
from decimal import Decimal


class Painterdetails(models.Model):
    _name='painter.details'

    
    name = fields.Char('Name')
    property_account_receivable = fields.Many2one('account.account',
            string="Account Payable",
            domain="[('type', '=', 'payable')]",
            help="This account will be used instead of the default one as the receivable account for the current partner",
            required=True)




class res_partner(models.Model):
    _inherit = 'res.partner'

    painter_id = fields.Many2one('painter.details', 'Painter')




class CommissionReport(models.Model):
    _name='commission.report'

    from_date=fields.Date(default=lambda self: self.default_time_range('from'),required=True)
    to_date=fields.Date(default=lambda self: self.default_time_range('to'),required=True)
    painter_id = fields.Many2one('painter.details', 'Painter',required=True)
    total_sale_amount = fields.Float('Total Sales',readonly=True)
    commission_per = fields.Float('Commission Persentage')
    commission_amount = fields.Float('Commission Amount')
    commission_in = fields.Boolean('Commission in Persentage')
    total_amount= fields.Float('Total Amount')

    
    @api.onchange('painter_id')
    def onchange_painter_id(self):
        painter = self.env['res.partner']
        amount = 0
        amount_credit = 0
        if self.painter_id:
            painter_ids = painter.search([('painter_id','=',self.painter_id.id)])
            print 'painterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr',painter_ids
            for cus_acc in  painter_ids:
                rec = self.env['account.move'].search([('date','>=',self.from_date),('date','<=',self.to_date),('state','in',['posted'])])
                print 'reffffffffffffffffffffffffffffffffffffffffffffffffff',rec
                for account in rec:
                    record = self.env['account.move.line'].search([('account_id','=',cus_acc.property_account_receivable.id)])
                    print 'reccccccccccccccccccccccccccccccccccccc',account.state
                    
                    for bal in record:
                        amount = bal.debit
                        amount_credit = bal.credit
                        self.total_sale_amount = self.total_sale_amount + amount -amount_credit









    @api.model
    def default_time_range(self, type):
        year = datetime.date.today().year
        month = datetime.date.today().month
        last_day = calendar.monthrange(datetime.date.today().year,datetime.date.today().month)[1]
        first_day = 1
        if type=='from':
            return datetime.date(year, month, first_day)
        elif type=='to':
            return datetime.date(year, month, last_day)